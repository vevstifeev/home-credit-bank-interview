
//        []{}() = true
//        [()]{} = true
//        [(]) = false
//        )( = false
//        [[()]] = true
//        [[(] = false
//        [([]{}[])] => true
//
//
//
//        Boolean CheckBalance(String input) {}

import java.util.*;

public class BalanceChecker {

    List<Character> validChars = Arrays.asList('{', '}', '(', ')', '[', ']');


    Map<Character, Character> bracetsMap = new HashMap<Character, Character>() {{
        put('{', '}');
        put('(', ')');
        put('[', ']');
    }};

    Boolean CheckBalance(String input) {
        if (input == null) {
            return false;
        }

        if (input.isEmpty()) {
            return true;
        }

        Stack<Character> openBrackets = new Stack<>();

        for (int i = 0; i < input.length(); i++) {
            char currentChar = input.charAt(i);
            if (!validChars.contains(currentChar)) {
                return false;
            }

            if (bracetsMap.containsKey(currentChar)) {
                openBrackets.push(currentChar);
                continue;
            }

            if (openBrackets.empty()) {
                return false;
            }

            Character lastOpenBracket = openBrackets.pop();
            Character expectedCloseBracket = bracetsMap.get(lastOpenBracket);
            if (expectedCloseBracket != currentChar) {
                return false;
            }
        }

        return openBrackets.empty();
    }
}
