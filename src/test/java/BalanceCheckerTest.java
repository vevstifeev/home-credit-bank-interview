import org.junit.Assert;
import org.junit.Test;

public class BalanceCheckerTest {

    @Test
    public void BalanceCheckerTest() {
        BalanceChecker balanceChecker = new BalanceChecker();

        Assert.assertEquals(true, balanceChecker.CheckBalance("[]{}()"));
        Assert.assertEquals(true, balanceChecker.CheckBalance("[()]{}"));
        Assert.assertEquals(false, balanceChecker.CheckBalance("[(])"));
        Assert.assertEquals(false, balanceChecker.CheckBalance(")("));
        Assert.assertEquals(true, balanceChecker.CheckBalance("[[()]]"));
        Assert.assertEquals(false, balanceChecker.CheckBalance("[[(]"));
        Assert.assertEquals(true, balanceChecker.CheckBalance("[([]{}[])]"));
    }
}
